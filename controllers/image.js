const fs = require('fs')
const path = require('path')


module.exports = {
    index(req, res) {
        // res.send(`The image:index controller ${req.params.image_id}`);
        // res.render('image');
        const ViewModel = {
            image: {
                uniqueId: 1,
                title: 'Sample Image 1',
                description: 'This is a sample.',
                filename: 'sample1.jpg',
                Views: 0,
                likes: 0,
                timestamp: Date.now()
            },
            comments: [{
                image_id: 1,
                email: 'test@testing.com',
                name: 'Test Tester',
                gravatar: 'http://lorempixel.com/75/75/animals/1',
                comment: 'This is a test comment...',
                timestamp: Date.now()
            }, {
                image_id: 1,
                email: 'test@testing.com',
                name: 'Test Tester',
                gravatar: 'http://lorempixel.com/75/75/animals/2',
                comment: 'Another followup comment!',
                timestamp: Date.now()
            }]
        };
        res.render('image', ViewModel);
    },

    create(req, res) {
        // res.send('the image:create POST controller');
        // res.redirect('/images/1');
        const saveImage = function () {
            let possible = 'abcdefghijklmnopqrstuvwxyz0123456789';
            let imgUrl = '';

            for (let i = 0; i < 6; i++) {
                imgUrl += possible.charAt(Math.floor(Math.random() * possible.length));
            }
            let a = 1;

            debugger;
            const tempPath = req.files.file.path;
            const ext = path.extname(req.files.file.name).toLowerCase();
            const targetPath = path.resolve(`./public/upload/${imgUrl}${ ext}`);

            if (ext === '.png' || ext === '.jpg' || ext === '.jpeg' || ext ===
                '.gif') {
                fs.rename(tempPath, targetPath, (err) => {
                    if (err) throw err;
                    res.redirect(`/images/ ${imgUrl}`);
                });
            } else {
                fs.unlink(tempPath, () => {
                    if (err) throw err;
                    res.json(500, {
                        error: 'Only image files are allowed.'
                    });
                });
            }
        }
        saveImage()
    },

    like(req, res) {
        res.send('The image:like POST controller');
    },

    comment(req, res) {
        res.send('The image:comment POST controller');
    }
}